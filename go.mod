module bitbucket.org/uwaploe/go-must

require (
	bitbucket.org/uwaploe/go-dvl v0.9.6
	bitbucket.org/uwaploe/go-ins v1.5.0
	bitbucket.org/uwaploe/go-ins/v2 v2.0.0
	bitbucket.org/uwaploe/go-svs v0.2.1
	bitbucket.org/uwaploe/navsvc v1.4.1
	github.com/vmihailenco/msgpack v4.0.4+incompatible
)

go 1.13
